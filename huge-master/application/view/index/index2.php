<!DOCTYPE html>
<html>
<head>
    <title>Mastermind</title>

</head>
<body onload="randomcolor(),createGrid()">

<div class="game">
    <button class="buttons" id="red" onclick="UserInput('red')"></button>
    <button class="buttons" id="orange" onclick="UserInput('orange')"></button>
    <button class="buttons" id="yellow" onclick="UserInput('yellow')"></button>
    <button class="buttons" id="green" onclick="UserInput('green')"></button>
    <button class="buttons" id="blue" onclick="UserInput('blue')"></button>
    <button class="buttons" id="black" onclick="UserInput('black')"></button>
    <button class="buttons" id="white" onclick="UserInput('white')"></button>
    <button class="buttons" id="purple" onclick="UserInput('purple')"></button>
</div>
<div id="playField">

</div>
<script src="<?php echo Config::get('URL'); ?>js/main.js"></script>
</body>
</html>

